from copy import deepcopy
from .mff_agents import grid_search_main
from main import save_structures

def filter_inner_unreachable(structures):

  def remove_connector_node_tiles(level):
    # Remove connector node symbols
    for i in range(len(level)):
      for j in range(len(level[i])):
        if level[i][j] == 'v' or level[i][j] == '^' or level[i][j] == '>' or level[i][j] == '<':
          level[i][j] = '-'

  def get_tiles_mario_can_stand_on(tiles):
    # Returns a list of solid tiles that have enough
    # room above so that Mario can stand on them

    def tiles_above_are_non_solid(current_tile):
      # Mario is 2 tiles tall so this function returns True
      # if current_tile has 2 Non-Solid tiles above

      # This function returns True if current_tile.r == 0
      if current_tile.r == 1:
        # Check that the one tile above is Non-Solid
        for tile in tiles:
          if tile.r == 0 and tile.c == current_tile.c:
            if tile.type == "Solid":
              return False
      elif current_tile.r >= 2:
        # Check that both tiles above are Non-Solid
        for tile in tiles:
          if (tile.r == current_tile.r - 1 or tile.r == current_tile.r - 2) and tile.c == current_tile.c:
            if tile.type == "Solid":
              return False
        
      return True
      
    tiles_mario_can_stand_on = list()
    for tile in tiles:
      if tile.type == "Solid" and tiles_above_are_non_solid(tile):
        tiles_mario_can_stand_on.append(tile)

    return tiles_mario_can_stand_on

  def tiles_reachable(tile1, tile2, level):

    def level_to_string(level):
      level_string = ""

      for row in level:
        row_str = ""
        for tile in row:
          row_str += tile
        level_string += row_str + "\n"

      return level_string
    
    level_copy = deepcopy(level)

    #Subtract to make Mario stand on top of desired tile and not in it
    level_copy[tile1.r - 1][tile1.c] = 'M'
    level_copy[tile2.r - 1][tile2.c] = 'F'
    tiles_reachable = grid_search_main.findGridPathForLevelFromString(level_to_string(level_copy), 0, False)
    
    if not tiles_reachable: 
      return False

    #Subtract to make Mario stand on top of desired tile and not in it
    level_copy[tile1.r - 1][tile1.c] = 'F'
    level_copy[tile2.r - 1][tile2.c] = 'M'
    tiles_reachable = grid_search_main.findGridPathForLevelFromString(level_to_string(level_copy), 0, False)

    if not tiles_reachable: 
      return False

    return True

  def save_inner_unreachable(structure):
    # Debugging function that saves filtered out structures
    x = list()
    x.append(s)
    save_structures(s, s, x, "output/inner-unreachable-structures")

  inner_reachable_structures = list()
  for s in structures:
    level = s.level_representation()
    remove_connector_node_tiles(level)
    tiles_mario_can_stand_on = get_tiles_mario_can_stand_on(s.nodes)
    inner_unreachable = False

    # Structures with less than 2 tiles with are always inner reachable
    if len(tiles_mario_can_stand_on) >= 2:
      for i in range(0, len(tiles_mario_can_stand_on) - 1):
        if inner_unreachable: break

        for j in range(i+1, len(tiles_mario_can_stand_on)):
          if inner_unreachable: break

          tile1 = tiles_mario_can_stand_on[i]
          tile2 = tiles_mario_can_stand_on[j]

          if not tiles_reachable(tile1, tile2, level):
            save_inner_unreachable(s)
            inner_unreachable = True

      if inner_unreachable: continue

    inner_reachable_structures.append(s)

  return inner_reachable_structures
